package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;

import java.rmi.RemoteException;

public interface INhanVienDAO extends IDataAccessObject<NhanVien> {
    /**
     * Phương thức xử lý đăng nhập
     * @param maNhanVien
     * @param matKhau
     * @return NhanVien
     * @throws RemoteException
     */
    NhanVien dangNhap(String maNhanVien, String matKhau) throws Exception;

    /**
     * Phươgn thức cập nhật mật khẩu cho nhân viên
     * @param maNhanVien
     * @param matKhauMoi
     * @return boolean
     * @throws RemoteException
     */
    boolean capNhatMatKhau(String maNhanVien, String matKhauCu, String matKhauMoi) throws Exception;
}
