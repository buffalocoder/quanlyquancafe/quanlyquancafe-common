package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.entities.MonAn;

import java.rmi.RemoteException;
import java.util.List;

public interface IMonAnDAO extends IDataAccessObject<MonAn> {
    List<MonAn> findByMaDanhMuc(String maDanhMuc) throws RemoteException;
}
