package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.entities.Ban;

public interface IBanDAO extends IDataAccessObject<Ban> {
}
