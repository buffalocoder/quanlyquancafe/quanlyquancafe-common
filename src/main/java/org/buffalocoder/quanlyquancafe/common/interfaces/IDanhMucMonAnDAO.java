package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;

public interface IDanhMucMonAnDAO extends IDataAccessObject<DanhMucMonAn> {
}
