package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.exceptions.DAOException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IDataAccessObject<T> extends Remote {
    /**
     * Insert T
     *
     * @param t
     * @return
     */
    boolean add(T t) throws RemoteException;

    /**
     * Update T
     *
     * @param t
     * @return
     */
    boolean update(T t) throws RemoteException;

    /**
     * Delete T By Id
     *
     * @param id
     * @return
     */
    boolean deleteById(Object id) throws RemoteException;

    /**
     * Delete object
     * @param t
     * @return boolean
     * @throws RemoteException
     */
    boolean delete(T t) throws RemoteException;

    /**
     * Find T By Key
     *
     * @param key
     * @return
     */
    T find(Object key) throws RemoteException;

    /**
     * Find All
     *
     * @return
     */
    List<T> find() throws RemoteException;

    /**
     * Generate ID
     * @return
     * @throws RemoteException
     */
    String generateId() throws RemoteException;

    /**
     * Create index
     * @throws RemoteException
     */
    void createIndex() throws RemoteException;

    /**
     * Full Text Search
     * @param keyword
     * @return List<T>
     * @throws RemoteException
     */
    List<T> fullTextSearch(String keyword) throws RemoteException;
}
