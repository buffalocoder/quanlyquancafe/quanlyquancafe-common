package org.buffalocoder.quanlyquancafe.common.interfaces;

import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.Locale;

public interface IHoaDonDAO extends IDataAccessObject<HoaDon> {
    /**
     * Tìm hóa đơn theo mã bàn
     * @param maBan
     * @return HoaDon
     */
    HoaDon findHoaDonChuaThanhToanByMaBan(String maBan) throws RemoteException;
    double getTongDanhThu(LocalDate ngayThongKe) throws  RemoteException;
    int getTongSoDonHang() throws RemoteException;
    double getTongTienThuVe() throws RemoteException;
    double getTongDoanhThuTatCa() throws RemoteException;

}
