package org.buffalocoder.quanlyquancafe.common.exceptions;

public class DAOException extends Exception {
    public DAOException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
