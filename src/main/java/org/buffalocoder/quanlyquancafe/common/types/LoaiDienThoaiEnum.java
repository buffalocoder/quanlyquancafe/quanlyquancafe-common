package org.buffalocoder.quanlyquancafe.common.types;

public enum LoaiDienThoaiEnum {
    DI_DONG,
    NHA_RIENG,
    CO_QUAN,
    KHAC
}
