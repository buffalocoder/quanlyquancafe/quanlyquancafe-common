package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.types.LoaiDienThoaiEnum;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class    DienThoai implements Serializable {
    @Column(name = FieldNameConst.DIENTHOAI_LOAI)
    private LoaiDienThoaiEnum loai;

    @Column(name = FieldNameConst.DIENTHOAI_SODIENTHOAI)
    private String soDienThoai;

    // region Getter, Setter
    public LoaiDienThoaiEnum getLoai() {
        return loai;
    }

    public void setLoai(LoaiDienThoaiEnum loai) {
        this.loai = loai;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }
    // endregion

    // region Constructor
    public DienThoai() {
    }

    public DienThoai(LoaiDienThoaiEnum loai, String soDienThoai) {
        this.setLoai(loai);
        this.setSoDienThoai(soDienThoai);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DienThoai dienThoai = (DienThoai) o;

        return soDienThoai != null ? soDienThoai.equals(dienThoai.soDienThoai) : dienThoai.soDienThoai == null;
    }

    @Override
    public int hashCode() {
        return soDienThoai != null ? soDienThoai.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DienThoai{" +
                "loai='" + loai + '\'' +
                ", soDienThoai='" + soDienThoai + '\'' +
                '}';
    }
    // endregion
}
