package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class ChiTietHoaDon implements Serializable {
    @OneToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = FieldNameConst.MONAN_MAMONAN)
    private MonAn monAn;

    @Column(name = FieldNameConst.CHITIETHOADON_SOLUONG)
    private int soLuong;

    @Column(name = FieldNameConst.CHITIETHOADON_GIABAN)
    private double giaBan;

    @Column(name = FieldNameConst.CHITIETHOADON_THANHTIEN)
    private double thanhTien;

    // region Getter, Setter
    public MonAn getMonAn() {
        return monAn;
    }

    public void setMonAn(MonAn monAn) throws ValidateException {
        if (monAn != null) {
            this.monAn = monAn;
        } else throw new ValidateException("Món ăn không được rỗng");
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) throws ValidateException {
        if (soLuong > 0) {
            this.soLuong = soLuong;
        } else throw new ValidateException("Số lượng phải lớn hơn 0");
    }

    public double getGiaBan() {
        return giaBan;
    }

    public void setGiaBan(double giaBan) throws ValidateException {
        if (giaBan > 0) {
            this.giaBan = giaBan;
        } else throw new ValidateException("Giá bán phải lớn hơn 0");
    }

    public double getThanhTien() {
        this.thanhTien = this.getSoLuong() * this.getGiaBan();
        return thanhTien;
    }

    public void setThanhTien(double thanhTien) throws ValidateException {
        if (thanhTien > 0) {
            this.thanhTien = thanhTien;
        } else throw new ValidateException("Thành tiền phải lớn hơn 0");
    }
    // endregion

    // region Constructor
    public ChiTietHoaDon() {
    }

    public ChiTietHoaDon(MonAn monAn, int soLuong, double giaBan, double thanhTien) throws ValidateException {
        this.setMonAn(monAn);
        this.setSoLuong(soLuong);
        this.setGiaBan(giaBan);
        this.setThanhTien(thanhTien);
    }

    public ChiTietHoaDon(MonAn monAn, int soLuong) throws ValidateException {
        this.setMonAn(monAn);
        this.setSoLuong(soLuong);
        this.setGiaBan(monAn.getGiaBan());
        this.setThanhTien(this.getThanhTien());
    }

    // endregion

    // region Override methods
    @Override
    public String toString() {
        return "ChiTietHoaDon{" +
                "monAn=" + monAn +
                ", soLuong=" + soLuong +
                ", giaBan=" + giaBan +
                ", thanhTien=" + thanhTien +
                '}';
    }
    // endregion
}
