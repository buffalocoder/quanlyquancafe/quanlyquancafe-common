package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = CollectionNameConst.TABLE_COLLECTION)
public class Ban implements Serializable {
    @Id
    @Column(name = FieldNameConst.BAN_MABAN)
    private String maBan;

    @Column(name = FieldNameConst.BAN_TRANGTHAI)
    private boolean trangThai;

    @Column(name = FieldNameConst.BAN_TONGCHONGOI)
    private int tongChoNgoi;

    // region Getter, Setter
    public String getMaBan() {
        return maBan;
    }

    public void setMaBan(String maBan) throws ValidateException {
        if (maBan != null && !maBan.trim().isEmpty() && Validate.isPatternId(maBan.trim(), PatternIdConst.PREFIX_BAN)) {
            this.maBan = maBan;
        } else if (maBan == null || maBan.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã bàn");
        } else if (!Validate.isPatternId(maBan.trim(), PatternIdConst.PREFIX_BAN)) {
            throw new ValidateException("Mã bàn không đúng định dạng (Bxxxx)");
        }
    }

    public boolean isTrangThai() {
        return trangThai;
    }

    public void setTrangThai(boolean trangThai) {
        this.trangThai = trangThai;
    }

    public int getTongChoNgoi() {
        return tongChoNgoi;
    }

    public void setTongChoNgoi(int tongChoNgoi) throws ValidateException {
        if (tongChoNgoi > 0) {
            this.tongChoNgoi = tongChoNgoi;
        } else {
            throw new ValidateException("Tổng chỗ ngồi phải lớn hơn 0");
        }
    }
    // endregion

    // region Constructor
    public Ban() {
    }

    public Ban(String maBan, boolean trangThai, int tongChoNgoi) throws ValidateException {
        this.setMaBan(maBan);
        this.setTrangThai(trangThai);
        this.setTongChoNgoi(tongChoNgoi);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ban ban = (Ban) o;

        return maBan != null ? maBan.equals(ban.maBan) : ban.maBan == null;
    }

    @Override
    public int hashCode() {
        return maBan != null ? maBan.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Ban{" +
                "maBan='" + maBan + '\'' +
                ", trangThai=" + trangThai +
                ", tongChoNgoi=" + tongChoNgoi +
                '}';
    }
    // endregion
}
