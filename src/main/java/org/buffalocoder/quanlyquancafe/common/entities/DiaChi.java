package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DiaChi implements Serializable {
    @Column(name = FieldNameConst.DIACHI_DIACHICHITIET)
    private String diaChiChiTiet;

    @Column(name = FieldNameConst.DIACHI_KHUVUC)
    private String khuVuc;

    @Column(name = FieldNameConst.DIACHI_QUANHUYEN)
    private String quanHuyen;

    @Column(name = FieldNameConst.DIACHI_TINHTHANHPHO)
    private String tinhThanhPho;

    // region Getter, Setter
    public String getDiaChiChiTiet() {
        return diaChiChiTiet;
    }

    public void setDiaChiChiTiet(String diaChiChiTiet) {
        this.diaChiChiTiet = diaChiChiTiet;
    }

    public String getKhuVuc() {
        return khuVuc;
    }

    public void setKhuVuc(String khuVuc) {
        this.khuVuc = khuVuc;
    }

    public String getQuanHuyen() {
        return quanHuyen;
    }

    public void setQuanHuyen(String quanHuyen) {
        this.quanHuyen = quanHuyen;
    }

    public String getTinhThanhPho() {
        return tinhThanhPho;
    }

    public void setTinhThanhPho(String tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
    }
    // endregion

    // region Constructor
    public DiaChi() {
    }

    public DiaChi(String diaChiChiTiet, String khuVuc, String quanHuyen, String tinhThanhPho) {
        this.setDiaChiChiTiet(diaChiChiTiet);
        this.setKhuVuc(khuVuc);
        this.setQuanHuyen(quanHuyen);
        this.setTinhThanhPho(tinhThanhPho);
    }
    // endregion

    // region Override methods
    @Override
    public String toString() {
        return "DiaChi{" +
                "diaChiChiTiet='" + diaChiChiTiet + '\'' +
                ", khuVuc='" + khuVuc + '\'' +
                ", quanHuyen='" + quanHuyen + '\'' +
                ", tinhThanhPho='" + tinhThanhPho + '\'' +
                '}';
    }
    // endregion

    // region Methods
    public String getDiaChi() {
        return String.format("%s, %s, %s, %s", diaChiChiTiet, khuVuc, quanHuyen, tinhThanhPho);
    }
    // endregion
}
