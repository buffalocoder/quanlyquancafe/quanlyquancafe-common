package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = CollectionNameConst.BILL_COLLECTION)
public class HoaDon implements Serializable {
    @Id
    @Column(name = FieldNameConst.HOADON_MAHOADON)
    private String maHoaDon;

    @Column(name = FieldNameConst.HOADON_NGAYXUATHOADON)
    private LocalDate ngayXuatHoaDon;

    @Column(name = FieldNameConst.HOADON_GHICHU)
    private String ghiChu;

    @Column(name = FieldNameConst.HOADON_TONGTIEN)
    private Double tongTien;

    @Column(name = FieldNameConst.HOADON_TIENKHACHTRA)
    private Double tienKhachTra;

    @Column(name = FieldNameConst.HOADON_TIENTHUA)
    private Double tienThua;

    @Column(name = FieldNameConst.HOADON_GIAMGIA)
    private Double giamGia;

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = FieldNameConst.NHANVIEN_MANHANVIEN)
    private NhanVien nhanVien;

    @ElementCollection (fetch = FetchType.EAGER)
    @Column(name = FieldNameConst.HOADON_CHITIET)
    private Set<ChiTietHoaDon> chiTietHoaDons;

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = FieldNameConst.BAN_MABAN)
    private Ban ban;

    // region Getter, Setter
    public Double getGiamGia() {
        return giamGia == null ? 0 : giamGia;
    }

    public void setGiamGia(Double giamGia) {
        this.giamGia = giamGia;
    }

    public String getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(String maHoaDon) throws ValidateException {
        if (maHoaDon != null && !maHoaDon.trim().isEmpty() && Validate.isPatternId(maHoaDon.trim(), PatternIdConst.PREFIX_HOA_DON)) {
            this.maHoaDon = maHoaDon;
        } else if (maHoaDon == null || maHoaDon.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã hóa đơn");
        } else if (!Validate.isPatternId(maHoaDon.trim(), PatternIdConst.PREFIX_HOA_DON)) {
            throw new ValidateException("Mã hóa đơn không đúng định dạng (HDxxxx)");
        }
    }

    public LocalDate getNgayXuatHoaDon() {
        return ngayXuatHoaDon;
    }

    public void setNgayXuatHoaDon(LocalDate ngayXuatHoaDon) throws ValidateException {
        LocalDate currentDate = LocalDate.now();

        if (ngayXuatHoaDon == null) {
            throw new ValidateException("Vui lòng nhập ngày xuất hóa đơn");
        } else if (ngayXuatHoaDon.equals(currentDate) || ngayXuatHoaDon.isAfter(currentDate)) {
            this.ngayXuatHoaDon = ngayXuatHoaDon;
        } else {
            throw new ValidateException("Ngày xuất hóa đơn phải sau hoặc bằng ngày hiện tại");
        }
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Double getTongTien() {
        this.tongTien = 0.0;
        for (ChiTietHoaDon chiTietHoaDon : chiTietHoaDons) {
            this.tongTien += chiTietHoaDon.getThanhTien();
        }
        return tongTien;
    }

    public void setTongTien(Double tongTien) throws ValidateException {
        if (tongTien >= 0) {
            this.tongTien = tongTien;
        } else throw new ValidateException("Tổng tiền phải lớn hơn hoặc bằng 0");
    }

    public Double getTienKhachTra() {
        return tienKhachTra;
    }

    public void setTienKhachTra(Double tienKhachTra) throws ValidateException {
        if (tienKhachTra >= 0) {
            this.tienKhachTra = tienKhachTra;
        } else throw new ValidateException("Tiền khách trả phải lớn hơn hoặc bằng 0");
    }

    public Double getTienThua() {
        if (tienKhachTra >= thanhTien()) {
            tienThua = tienKhachTra - thanhTien();
        } else tienThua = 0.0;

        return tienThua;
    }

    public void setTienThua(Double tienThua) throws ValidateException {
        if (tienThua >= 0) {
            this.tienThua = tienThua;
        } else throw new ValidateException("Tiền thừa phải lớn hơn hoặc bằng 0");
    }

    public NhanVien getNhanVien() {
        return nhanVien;
    }

    public void setNhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
    }

    public Set<ChiTietHoaDon> getChiTietHoaDons() {
        return chiTietHoaDons;
    }

    public void setChiTietHoaDons(Set<ChiTietHoaDon> chiTietHoaDons) {
        this.chiTietHoaDons = chiTietHoaDons;
    }

    public Ban getBan() {
        return ban;
    }

    public void setBan(Ban ban) {
        this.ban = ban;
    }
    // endregion

    // region Constructor
    public HoaDon() {
    }

    public HoaDon(String maHoaDon, LocalDate ngayXuatHoaDon, String ghiChu, Double tongTien, Double tienKhachTra, Double tienThua, NhanVien nhanVien, Set<ChiTietHoaDon> chiTietHoaDons, Ban ban) throws ValidateException {
        this.setMaHoaDon(maHoaDon);
        this.setNgayXuatHoaDon(ngayXuatHoaDon);
        this.setGhiChu(ghiChu);
        this.setTongTien(tongTien);
        this.setTienKhachTra(tienKhachTra);
        this.setTienThua(tienThua);
        this.setNhanVien(nhanVien);
        this.setChiTietHoaDons(chiTietHoaDons);
        this.setBan(ban);
        this.setGiamGia(0.0);
    }

    public HoaDon(String maHoaDon, LocalDate ngayXuatHoaDon, String ghiChu, NhanVien nhanVien, Set<ChiTietHoaDon> chiTietHoaDons, Ban ban) throws ValidateException {
        this.setMaHoaDon(maHoaDon);
        this.setNgayXuatHoaDon(ngayXuatHoaDon);
        this.setGhiChu(ghiChu);
        this.setTongTien(0.0);
        this.setTienKhachTra(0.0);
        this.setTienThua(0.0);
        this.setNhanVien(nhanVien);
        this.setChiTietHoaDons(chiTietHoaDons);
        this.setBan(ban);
        this.setGiamGia(0.0);
    }

    public HoaDon(String maHoaDon, LocalDate ngayXuatHoaDon, String ghiChu, NhanVien nhanVien, Ban ban) throws ValidateException {
        this.setMaHoaDon(maHoaDon);
        this.setNgayXuatHoaDon(ngayXuatHoaDon);
        this.setGhiChu(ghiChu);
        this.setTongTien(0.0);
        this.setTienKhachTra(0.0);
        this.setTienThua(0.0);
        this.setNhanVien(nhanVien);
        this.setChiTietHoaDons(new HashSet<>());
        this.setBan(ban);
        this.setGiamGia(0.0);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HoaDon hoaDon = (HoaDon) o;

        return maHoaDon != null ? maHoaDon.equals(hoaDon.maHoaDon) : hoaDon.maHoaDon == null;
    }

    @Override
    public int hashCode() {
        return maHoaDon != null ? maHoaDon.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HoaDon{" +
                "maHoaDon='" + maHoaDon + '\'' +
                ", ngayXuatHoaDon=" + ngayXuatHoaDon +
                ", ghiChu='" + ghiChu + '\'' +
                ", tongTien=" + tongTien +
                ", tienKhachTra=" + tienKhachTra +
                ", tienThua=" + tienThua +
                ", giamGia=" + giamGia +
                ", nhanVien=" + nhanVien +
                ", chiTietHoaDons=" + chiTietHoaDons +
                ", ban=" + ban +
                '}';
    }
    // endregion

    // region Methods
    public double thanhTien() {
        return getTongTien() - getGiamGia();
    }
    // endregion
}
