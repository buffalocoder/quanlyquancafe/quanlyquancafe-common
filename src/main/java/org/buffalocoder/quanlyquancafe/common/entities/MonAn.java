package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = CollectionNameConst.FOOD_COLLECTION)
public class MonAn implements Serializable {
    @Id
    @Column(name = FieldNameConst.MONAN_MAMONAN)
    private String maMonAn;

    @Column(name = FieldNameConst.MONAN_TENMONAN)
    private String tenMonAn;

    @Column(name = FieldNameConst.MONAN_GIABAN)
    private double giaBan;

    @Column(name = FieldNameConst.MONAN_MOTA)
    private String moTa;

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = FieldNameConst.DANHMUC_MADANHMUC)
    private DanhMucMonAn danhMucMonAn;

    // region Getter, Setter
    public String getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(String maMonAn) throws ValidateException {
        if (maMonAn != null && !maMonAn.trim().isEmpty() && Validate.isPatternId(maMonAn.trim(), PatternIdConst.PREFIX_MON_AN)) {
            this.maMonAn = maMonAn;
        } else if (maMonAn == null || maMonAn.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã món ăn");
        } else if (!Validate.isPatternId(maMonAn.trim(), PatternIdConst.PREFIX_MON_AN)) {
            throw new ValidateException("Mã món ăn không đúng định dạng (MAxxxx)");
        }
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) throws ValidateException {
        if (tenMonAn != null && !tenMonAn.trim().isEmpty()) {
            this.tenMonAn = tenMonAn;
        } else throw new ValidateException("Vui lòng nhập tên món ăn");
    }

    public double getGiaBan() {
        return giaBan;
    }

    public void setGiaBan(double giaBan) throws ValidateException {
        if (giaBan > 0) {
            this.giaBan = giaBan;
        } else throw new ValidateException("Giá bán phải lớn hơn 0");
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public DanhMucMonAn getDanhMucMonAn() {
        return danhMucMonAn;
    }

    public void setDanhMucMonAn(DanhMucMonAn danhMucMonAn) {
        this.danhMucMonAn = danhMucMonAn;
    }
    // endregion

    // region Constructor
    public MonAn() {
    }

    public MonAn(String maMonAn, String tenMonAn, double giaBan, String moTa, DanhMucMonAn danhMucMonAn) throws ValidateException {
        this.setMaMonAn(maMonAn);
        this.setTenMonAn(tenMonAn);
        this.setGiaBan(giaBan);
        this.setMoTa(moTa);
        this.setDanhMucMonAn(danhMucMonAn);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MonAn monAn = (MonAn) o;

        return maMonAn != null ? maMonAn.equals(monAn.maMonAn) : monAn.maMonAn == null;
    }

    @Override
    public int hashCode() {
        return maMonAn != null ? maMonAn.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "MonAn{" +
                "maMonAn='" + maMonAn + '\'' +
                ", tenMonAn='" + tenMonAn + '\'' +
                ", giaBan=" + giaBan +
                ", moTa='" + moTa + '\'' +
                ", danhMucMonAn=" + danhMucMonAn +
                '}';
    }
    // endregion
}
