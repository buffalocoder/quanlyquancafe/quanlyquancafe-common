package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiNhanVienEnum;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;
import org.mindrot.jbcrypt.BCrypt;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = CollectionNameConst.EMPLOYEE_COLLECTION)
public class NhanVien implements Serializable {
    @Id
    @Column(name = FieldNameConst.NHANVIEN_MANHANVIEN)
    private String maNhanVien;

    @Column(name = FieldNameConst.NHANVIEN_TENNHANVIEN)
    private String tenNhanVien;

    @Column(name = FieldNameConst.NHANVIEN_MATKHAU)
    private String matKhau;

    @Column(name = FieldNameConst.NHANVIEN_GIOITINH)
    private GioiTinhEnum gioiTinh;

    @Column(name = FieldNameConst.NHANVIEN_NGAYSINH)
    private LocalDate ngaySinh;

    @Column(name = FieldNameConst.NHANVIEN_EMAIL)
    private String email;

    @Column(name = FieldNameConst.NHANVIEN_NGAYVAOLAM)
    private LocalDate ngayVaoLam;

    @Column(name = FieldNameConst.NHANVIEN_LOAINHANVIEN)
    private LoaiNhanVienEnum loaiNhanVien;

    @Column(name = FieldNameConst.NHANVIEN_CMND)
    private String chungMinhNhanDan;

    @Embedded
    @Column(name = FieldNameConst.NHANVIEN_DIACHI)
    private DiaChi diaChi;

    @Embedded
    @Column(name = FieldNameConst.NHANVIEN_DIENTHOAI)
    private DienThoai dienThoai;

    // region Getter, Setter
    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) throws ValidateException {
        if (maNhanVien != null && !maNhanVien.trim().isEmpty() && Validate.isPatternId(maNhanVien.trim(), PatternIdConst.PREFIX_NHAN_VIEN)) {
            this.maNhanVien = maNhanVien;
        } else if (maNhanVien == null || maNhanVien.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã nhân viên");
        } else if (!Validate.isPatternId(maNhanVien.trim(), PatternIdConst.PREFIX_NHAN_VIEN)) {
            throw new ValidateException("Mã nhân viên không đúng định dạng (NVxxxx)");
        }
    }

    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public void setTenNhanVien(String tenNhanVien) throws ValidateException {
        if (tenNhanVien != null && !tenNhanVien.trim().isEmpty()) {
            this.tenNhanVien = tenNhanVien;
        } else throw new ValidateException("Vui lòng nhập tên nhân viên");
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) throws ValidateException {
        if (matKhau != null && !matKhau.trim().isEmpty() && matKhau.trim().length() >= 8) {
            this.matKhau = BCrypt.hashpw(matKhau, BCrypt.gensalt(12));
        } else if (matKhau == null || matKhau.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mật khẩu");
        } else if (matKhau.trim().length() < 8) {
            throw new ValidateException("Mật khẩu phải trên 8 kí tự");
        }
    }

    public String getGioiTinh() {
        if (gioiTinh == GioiTinhEnum.NU) {
            return "Nữ";
        } else if (gioiTinh == GioiTinhEnum.NAM) {
            return "Nam";
        } else return "Không xác định";
    }

    public void setGioiTinh(GioiTinhEnum gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) throws ValidateException {
        LocalDate currentDate = LocalDate.now();
        LocalDate minDate = LocalDate.of(currentDate.getYear() - 40, 1, 1);
        LocalDate maxDate = LocalDate.of(currentDate.getYear() - 16, 1, 1);

        if (ngaySinh == null) {
            throw new ValidateException("Vui lòng nhập ngày sinh");
        } else if (!ngaySinh.isBefore(maxDate) || !ngaySinh.isAfter(minDate)) {
            throw new ValidateException("Nhân viên chưa đủ độ tuổi");
        } else {
            this.ngaySinh = ngaySinh;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws ValidateException {
        if (email != null && !email.trim().isEmpty() && Validate.isEmail(email.trim())) {
            this.email = email;
        } else if (email == null || !email.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập email");
        } else throw new ValidateException("Email không đúng định dạng");
    }

    public LocalDate getNgayVaoLam() {
        return ngayVaoLam;
    }

    public void setNgayVaoLam(LocalDate ngayVaoLam) throws ValidateException {
        LocalDate currentDate = LocalDate.now();

        if (ngayVaoLam == null) {
            throw new ValidateException("Vui lòng nhập ngày vào làm");
        } else if (ngayVaoLam.equals(currentDate) || ngayVaoLam.isAfter(currentDate)) {
            this.ngayVaoLam = ngayVaoLam;
        } else {
            throw new ValidateException("Ngày vào làm phải sau hoặc bằng ngày hiện tại");
        }
    }

    public LoaiNhanVienEnum getLoaiNhanVien() {
        return loaiNhanVien;
    }

    public void setLoaiNhanVien(LoaiNhanVienEnum loaiNhanVien) {
        this.loaiNhanVien = loaiNhanVien;
    }

    public String getChungMinhNhanDan() {
        return chungMinhNhanDan;
    }

    public void setChungMinhNhanDan(String chungMinhNhanDan) throws ValidateException {
        if (chungMinhNhanDan != null && !chungMinhNhanDan.trim().isEmpty() && Validate.isCMND(chungMinhNhanDan.trim())) {
            this.chungMinhNhanDan = chungMinhNhanDan;
        } else if (chungMinhNhanDan == null || !chungMinhNhanDan.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập chứng minh nhân dân");
        } else throw new ValidateException("Chứng minh nhân dân không đúng định dạng");
    }

    public DiaChi getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(DiaChi diaChi) {
        this.diaChi = diaChi;
    }

    public DienThoai getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(DienThoai dienThoai) {
        this.dienThoai = dienThoai;
    }
    // endregion

    // region Constructor
    public NhanVien() {
    }

    public NhanVien(String maNhanVien, String tenNhanVien, String matKhau, GioiTinhEnum gioiTinh, LocalDate ngaySinh, String email,
                    LocalDate ngayVaoLam, LoaiNhanVienEnum loaiNhanVien, String chungMinhNhanDan, DiaChi diaChi, DienThoai dienThoai)
            throws ValidateException {
        this.setMaNhanVien(maNhanVien);
        this.setTenNhanVien(tenNhanVien);
        this.setMatKhau(matKhau);
        this.setGioiTinh(gioiTinh);
        this.setNgaySinh(ngaySinh);
        this.setEmail(email);
        this.setNgayVaoLam(ngayVaoLam);
        this.setLoaiNhanVien(loaiNhanVien);
        this.setChungMinhNhanDan(chungMinhNhanDan);
        this.setDiaChi(diaChi);
        this.setDienThoai(dienThoai);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NhanVien nhanVien = (NhanVien) o;

        return maNhanVien != null ? maNhanVien.equals(nhanVien.maNhanVien) : nhanVien.maNhanVien == null;
    }

    @Override
    public int hashCode() {
        return maNhanVien != null ? maNhanVien.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NhanVien{" +
                "maNhanVien='" + maNhanVien + '\'' +
                ", tenNhanVien='" + tenNhanVien + '\'' +
                ", matKhau='" + matKhau + '\'' +
                ", gioiTinh=" + gioiTinh +
                ", ngaySinh=" + ngaySinh +
                ", email='" + email + '\'' +
                ", ngayVaoLam=" + ngayVaoLam +
                ", loaiNhanVien=" + loaiNhanVien +
                ", cmnd='" + chungMinhNhanDan + '\'' +
                ", diaChi=" + diaChi +
                ", dienThoai=" + dienThoai +
                '}';
    }
    // endregion

    // region Methods
    public boolean checkPassword(String matKhau) {
        return BCrypt.checkpw(matKhau, this.getMatKhau());
    }
    // endregion
}
