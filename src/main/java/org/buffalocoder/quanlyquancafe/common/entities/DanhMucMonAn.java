package org.buffalocoder.quanlyquancafe.common.entities;

import org.buffalocoder.quanlyquancafe.common.consts.CollectionNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.consts.PatternIdConst;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = CollectionNameConst.CATEGORY_COLLECTION)
public class DanhMucMonAn implements Serializable {
    @Id
    @Column(name = FieldNameConst.DANHMUC_MADANHMUC)
    private String maDanhMuc;

    @Column(name = FieldNameConst.DANHMUC_TENDANHMUC)
    private String tenDanhMuc;

    // region Getter, Setter
    public String getMaDanhMuc() {
        return maDanhMuc;
    }

    public void setMaDanhMuc(String maDanhMuc) throws ValidateException {
        if (maDanhMuc != null && !maDanhMuc.trim().isEmpty() && Validate.isPatternId(maDanhMuc.trim(), PatternIdConst.PREFIX_DANH_MUC)) {
            this.maDanhMuc = maDanhMuc;
        } else if (maDanhMuc == null || maDanhMuc.trim().isEmpty()) {
            throw new ValidateException("Vui lòng nhập mã danh mục");
        } else if (!Validate.isPatternId(maDanhMuc.trim(), PatternIdConst.PREFIX_DANH_MUC)) {
            throw new ValidateException("Mã danh mục không đúng định dạng (DMxxxx)");
        }
    }

    public String getTenDanhMuc() {
        return tenDanhMuc;
    }

    public void setTenDanhMuc(String tenDanhMuc) throws ValidateException {
        if (tenDanhMuc != null && !tenDanhMuc.trim().isEmpty()) {
            this.tenDanhMuc = tenDanhMuc;
        } else throw new ValidateException("Vui lòng nhập tên danh mục");
    }
    // endregion

    // region Constructor
    public DanhMucMonAn() {
    }

    public DanhMucMonAn(String maDanhMuc, String tenDanhMuc) throws ValidateException {
        this.setMaDanhMuc(maDanhMuc);
        this.setTenDanhMuc(tenDanhMuc);
    }
    // endregion

    // region Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DanhMucMonAn that = (DanhMucMonAn) o;

        return maDanhMuc != null ? maDanhMuc.equals(that.maDanhMuc) : that.maDanhMuc == null;
    }

    @Override
    public int hashCode() {
        return maDanhMuc != null ? maDanhMuc.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DanhMucMonAn{" +
                "maDanhMuc='" + maDanhMuc + '\'' +
                ", tenDanhMuc='" + tenDanhMuc + '\'' +
                '}';
    }
    // endregion
}
