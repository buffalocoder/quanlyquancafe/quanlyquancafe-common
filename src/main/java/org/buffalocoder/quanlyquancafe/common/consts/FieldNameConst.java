package org.buffalocoder.quanlyquancafe.common.consts;

public class FieldNameConst {
    // region Ban
    public static final String BAN_MABAN = "maBan";
    public static final String BAN_TRANGTHAI = "trangThai";
    public static final String BAN_TONGCHONGOI = "tongChoNgoi";
    // endregion

    // region Chi tiet hoa don
    public static final String CHITIETHOADON_SOLUONG = "soLuong";
    public static final String CHITIETHOADON_GIABAN = "giaBan";
    public static final String CHITIETHOADON_THANHTIEN = "thanhTien";
    // endregion

    // region Danh muc
    public static final String DANHMUC_MADANHMUC = "maDanhMuc";
    public static final String DANHMUC_TENDANHMUC = "tenDanhMuc";
    // endregion

    // region Dia chi
    public static final String DIACHI_DIACHICHITIET = "diaChiChiTiet";
    public static final String DIACHI_KHUVUC = "khuVuc";
    public static final String DIACHI_QUANHUYEN = "quanHuyen";
    public static final String DIACHI_TINHTHANHPHO = "tinhThanhPho";
    // endregion

    // region Dien thoai
    public static final String DIENTHOAI_LOAI = "loai";
    public static final String DIENTHOAI_SODIENTHOAI = "soDienThoai";
    // endregion

    // region Hoa don
    public static final String HOADON_MAHOADON = "maHoaDon";
    public static final String HOADON_NGAYXUATHOADON = "ngayXuatHoaDon";
    public static final String HOADON_GHICHU = "ghiChu";
    public static final String HOADON_TONGTIEN = "tongTien";
    public static final String HOADON_TIENKHACHTRA = "tienKhachTra";
    public static final String HOADON_TIENTHUA = "tienThua";
    public static final String HOADON_CHITIET = "chiTietHoaDons";
    public static final String HOADON_GIAMGIA = "giamGia";
    // endregion

    // region Mon an
    public static final String MONAN_MAMONAN = "maMonAn";
    public static final String MONAN_TENMONAN = "tenMonAn";
    public static final String MONAN_GIABAN = "giaBan";
    public static final String MONAN_MOTA = "moTa";
    // endregion

    // region Nhan vien
    public static final String NHANVIEN_MANHANVIEN = "maNhanVien";
    public static final String NHANVIEN_TENNHANVIEN = "tenNhanVien";
    public static final String NHANVIEN_MATKHAU = "matKhau";
    public static final String NHANVIEN_GIOITINH = "gioiTinh";
    public static final String NHANVIEN_NGAYSINH = "ngaySinh";
    public static final String NHANVIEN_EMAIL = "email";
    public static final String NHANVIEN_NGAYVAOLAM = "ngayVaoLam";
    public static final String NHANVIEN_LOAINHANVIEN = "loaiNhanVien";
    public static final String NHANVIEN_CMND = "chungMinhNhanDan";
    public static final String NHANVIEN_DIACHI = "diaChi";
    public static final String NHANVIEN_DIENTHOAI = "dienThoai";
    // endregion
}
