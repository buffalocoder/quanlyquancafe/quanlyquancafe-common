package org.buffalocoder.quanlyquancafe.common.consts;

public class PatternIdConst {
    public static final String PREFIX_BAN = "B";
    public static final String PREFIX_DANH_MUC = "DM";
    public static final String PREFIX_HOA_DON = "HD";
    public static final String PREFIX_MON_AN = "MA";
    public static final String PREFIX_NHAN_VIEN = "NV";
}
