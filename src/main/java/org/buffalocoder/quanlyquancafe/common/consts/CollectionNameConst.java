package org.buffalocoder.quanlyquancafe.common.consts;

public class CollectionNameConst {
    public static final String TABLE_COLLECTION = "tables";
    public static final String CATEGORY_COLLECTION = "categories";
    public static final String FOOD_COLLECTION = "foods";
    public static final String EMPLOYEE_COLLECTION = "employees";
    public static final String BILL_COLLECTION = "bills";
}
